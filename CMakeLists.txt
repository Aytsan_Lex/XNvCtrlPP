cmake_minimum_required(VERSION 3.5)

project(XNvCtrlpp LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

file(GLOB_RECURSE HEADERS include/*.hpp)
file(GLOB_RECURSE SOURCES src/*.cpp)

include_directories("./include")

add_library(${PROJECT_NAME}
    STATIC
    ${HEADERS}
    ${SOURCES}
)

# Link with native X11-Xlib and XNVCtrl
target_link_libraries(${PROJECT_NAME} X11 XNVCtrl)
