#pragma once

#include <string>

namespace Xorg_lib
{
#include <X11/Xlib.h>
namespace XNVCtrl
{
#include <NVCtrl/NVCtrl.h>
#include <NVCtrl/NVCtrlLib.h>
} // namespace Xorg_lib::XNVCtrl
} // namespace Xorg_lib

class XNvCtrlPP
{
public:
    struct GPU_static_info
    {
        std::string name;
        std::string uuid;
        std::string vbios_ver;
        std::string driver_ver;

        unsigned total_memory;
        unsigned slowdown_temperature;
        unsigned shutdown_temperature;
        unsigned pci_bus;
        unsigned pci_device;
        unsigned pci_function;

        unsigned max_screen_width;
        unsigned max_screen_height;
        unsigned max_displays;
    };

    struct GPU_dynamic_info
    {
        unsigned power_mizer_mode;
        unsigned core_temperature;
        unsigned fan_speed_level;
        unsigned fan_speed_rpm;
    };

public:
    XNvCtrlPP();
    XNvCtrlPP(const char* display_name); // ":0", ":1" etc... see echo $DISPLAY, default is ":0"
    ~XNvCtrlPP();

    std::string get_GPU_name();
    std::string get_GPU_VBIOS_version();
    std::string get_GPU_UUID();
    std::string get_GPU_driver_version();
    std::string get_GPU_current_clock_freq();

    unsigned get_GPU_total_memory() const;
    unsigned get_GPU_slowdown_threshold() const;
    unsigned get_GPU_shutdown_threshold() const;
    unsigned get_GPU_pci_bus() const;
    unsigned get_GPU_pci_device() const;
    unsigned get_GPU_pci_function() const;
    unsigned get_GPU_max_screen_width() const;
    unsigned get_GPU_max_screen_height() const;
    unsigned get_GPU_max_displays() const;

    unsigned get_GPU_power_mizer_mode() const;
    unsigned get_GPU_core_temperature() const;
    unsigned get_GPU_fan_speed_level() const;
    unsigned get_GPU_fan_speed_rpm() const;

    GPU_static_info get_GPU_static_info();
    GPU_dynamic_info get_GPU_dynamic_info();

private:
    Xorg_lib::Display* display_;

    char* read_string_attribute(unsigned attribute_id);
    char* read_string_attribute(unsigned target_type, unsigned attribute_id);
    int read_int_attribute(unsigned attribute_id) const;
    int read_int_attribute(unsigned target_type, unsigned attribute_id) const;
};
